"use strict";

let x = "";
let y = "";
let operation = "";

do {
  x = prompt("Enter number X", x);
  y = prompt("Enter number Y", y);
  operation = prompt("Enter a sign of operation", operation);
} while (isNaN(x) || isNaN(y) || x.trim() === "" || y.trim() === "");

function calcXY(x, y, operation) {
  switch (operation) {
    case "+":
      return +x + +y;
    case "-":
      return x - y;
    case "*":
      return x * y;
    case "/":
      return x / y;
  }
}
console.log(calcXY(x, y, operation));
